package com.recruit2022964.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DoubleDataTest {

    @Test
    public void testAdd() {
        DoubleData result = new DoubleData(12.3).merge(new DoubleData(1.));
        assertEquals(new DoubleData(13.3), result);
    }

    @Test
    public void testCompareTo() {
        assertEquals(1, new DoubleData(12.3).compareTo(new DoubleData(1.)));
        assertEquals(-1, new DoubleData(2.).compareTo(new DoubleData(123.)));
        assertEquals(0, new DoubleData(1.).compareTo(new DoubleData(1.)));
    }
}
