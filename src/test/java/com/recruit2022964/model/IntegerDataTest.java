package com.recruit2022964.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class IntegerDataTest {

    @Test
    public void testAdd() {
        IntegerData result = new IntegerData(123).merge(new IntegerData(1));
        assertEquals(new IntegerData(124), result);
    }

    @Test
    public void testCompareTo() {
        assertEquals(1, new IntegerData(123).compareTo(new IntegerData(1)));
        assertEquals(-1, new IntegerData(2).compareTo(new IntegerData(123)));
        assertEquals(0, new IntegerData(1).compareTo(new IntegerData(1)));
    }

}
