package com.recruit2022964;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.recruit2022964.repo.DataRepository;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Recruit2022964ApplicationTests {

    @Autowired
    private Map<String, DataRepository> dataRepository;

    @Test
    void contextLoads() {
        assertThat(dataRepository).isNotNull();
        assertEquals(4, dataRepository.size());
    }

}
