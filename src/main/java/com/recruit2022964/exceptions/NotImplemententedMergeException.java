package com.recruit2022964.exceptions;

/**
 *
 * @author Małgorzata Sienkiewicz
 */
public class NotImplemententedMergeException extends RuntimeException {

    public NotImplemententedMergeException(Class clazz1, Class clazz2) {
        super("Merging " + clazz1 + " with " + clazz2 + " is not implemented.");
    }
}
