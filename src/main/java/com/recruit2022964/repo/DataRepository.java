package com.recruit2022964.repo;

import com.recruit2022964.model.DataMerger;

public interface DataRepository<T extends DataMerger> {

    T get() throws Exception;
}
