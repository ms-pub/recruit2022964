package com.recruit2022964.repo.random;

import com.recruit2022964.model.DoubleData;
import com.recruit2022964.repo.DataRepository;
import java.security.SecureRandom;
import java.util.Random;
import org.springframework.stereotype.Repository;

@Repository("doubleRandom")
class DoubleRandomDataRepository implements DataRepository<DoubleData> {

    private final Random random = new SecureRandom();

    @Override
    public DoubleData get() {
        return new DoubleData(random.nextDouble());
    }

}
