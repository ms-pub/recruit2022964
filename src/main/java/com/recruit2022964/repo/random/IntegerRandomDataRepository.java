package com.recruit2022964.repo.random;

import com.recruit2022964.model.IntegerData;
import com.recruit2022964.repo.DataRepository;
import java.security.SecureRandom;
import java.util.Random;
import org.springframework.stereotype.Repository;

@Repository("integerRandom")
class IntegerRandomDataRepository implements DataRepository<IntegerData> {

    private final Random random = new SecureRandom();

    @Override
    public IntegerData get() {
        return new IntegerData(random.nextInt());
    }

}
