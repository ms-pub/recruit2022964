package com.recruit2022964.repo.randomorg;

import com.recruit2022964.model.IntegerData;
import com.recruit2022964.repo.DataRepository;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

@Repository("integerRandomOrg")
@PropertySource("randomorg.properties")
class IntegerRandomOrgDataRepository extends RandomOrgDataRepository implements DataRepository<IntegerData> {

    @Value("${randomorg.integer.url}")
    private String integetUrl;

    @Override
    public IntegerData get() throws IOException {
        return new IntegerData(Integer.valueOf(getFromRadnomOrg(integetUrl)));
    }

}
