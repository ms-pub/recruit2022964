package com.recruit2022964.repo.randomorg;

import com.recruit2022964.model.DoubleData;
import com.recruit2022964.repo.DataRepository;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

@Repository("doubleRandomOrg")
@PropertySource("randomorg.properties")
class DoubleRandomOrgDataRepository extends RandomOrgDataRepository implements DataRepository<DoubleData> {

    @Value("${randomorg.integer.url}")
    private String integetUrl;
    @Value("${randomorg.decimal.url}")
    private String decimalUrl;

    @Override
    public DoubleData get() throws IOException {
        Double dec = Double.valueOf(getFromRadnomOrg(decimalUrl));
        Double i = Double.valueOf(getFromRadnomOrg(integetUrl));
        return new DoubleData(i + dec);
    }

}
