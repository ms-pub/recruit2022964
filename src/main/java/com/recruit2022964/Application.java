package com.recruit2022964;

import com.recruit2022964.exceptions.NotImplemententedMergeException;
import com.recruit2022964.model.DataMerger;
import com.recruit2022964.repo.DataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            printUsage();
        } else {
            ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
            DataRepository repo1;
            DataRepository repo2;
            try {
                repo1 = (DataRepository) context.getBean(args[0]);
            } catch (NoSuchBeanDefinitionException | ClassCastException ex) {
                LOG.error("No such data source {}.", args[0]);
                printUsage();
                return;
            }
            try {
                repo2 = (DataRepository) context.getBean(args[1]);
            } catch (NoSuchBeanDefinitionException | ClassCastException ex) {
                LOG.error("No such data source {}.", args[1]);
                printUsage();
                return;
            }
            DataMerger i1 = repo1.get();
            DataMerger i2 = repo2.get();
            try {
                DataMerger m = i1.merge(i2);
                LOG.info("{} + {} = {}", i1, i2, m);
            } catch (NotImplemententedMergeException ex) {
                LOG.error("Merging is not implemented.", ex);
            }
        }

    }

    private static void printUsage() {
        LOG.info("Usage:\n"
            + "\t- give two data sources,\n"
            + "\t- available are:\n"
            + "\t\t- doubleRandom,\n"
            + "\t\t- integerRandom,\n"
            + "\t\t- doubleRandomOrg,\n"
            + "\t\t- integerRandomOrg.");
    }

}
