package com.recruit2022964.model;

import com.recruit2022964.exceptions.NotImplemententedMergeException;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DoubleData implements DataMerger<DoubleData>, Comparable<DoubleData> {

    private final Double value;

    @Override
    public DoubleData merge(DataMerger o) {
        if (o.getClass() != DoubleData.class) {
            throw new NotImplemententedMergeException(DoubleData.class, o.getClass());
        }
        return new DoubleData(value + ((DoubleData) o).value);
    }

    @Override
    public int compareTo(DoubleData o) {
        return this.value.compareTo(o.value);
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
