package com.recruit2022964.model;

import com.recruit2022964.exceptions.NotImplemententedMergeException;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IntegerData implements DataMerger<IntegerData>, Comparable<IntegerData> {

    private final Integer value;

    @Override
    public IntegerData merge(DataMerger o) {
        if (o.getClass() != IntegerData.class) {
            throw new NotImplemententedMergeException(IntegerData.class, o.getClass());
        }
        return new IntegerData(value + ((IntegerData) o).value);
    }

    @Override
    public int compareTo(IntegerData o) {
        return this.value.compareTo(o.value);
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
