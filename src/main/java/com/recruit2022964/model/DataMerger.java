package com.recruit2022964.model;

public interface DataMerger<T> {

    DataMerger<T> merge(DataMerger o);

}
